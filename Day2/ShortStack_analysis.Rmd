---
title: "Introduction to sRNAseq data visualisation in R"
author: "Todd Blevins, David Pflieger"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = '~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_analysis/ShortStack')
```

For our purpose in this tutorial, we are going to need the following packages.

```{r libraries, message=FALSE}
library(dplyr) # help splitting, applying and combining data
library(plyr) # help for data manipulation
library(data.table) # extension to data.frame format, optimized for huge datasets
library(ggplot2) # The grammar of graphics, it improves the quality and aesthetic of your graphs
```

## Set the working directory

The files we need for this analysis are stored in the /ShortStack folder we copied before.
We will set the working directory using the full path of this directory.

```{r working directory, eval = F}
?setwd
setwd("~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_analysis/ShortStack/")
getwd()
```

## Load ShortStack sRNAseq results into R

Now, we will load the 3 text files results generated by ShortStack and concat them into a single table.
To load those results files, R needs to know where they are actually stored. 
If you have correctly set up your working directory, you should have them here:

- WT_sRNA_shortstack/Results.txt
- nrpd1_sRNA_shortstack/Results.txt
- nrpe1_sRNA_shortstack/Results.txt

We are going to load them with the **fread** command (?fread). It is an optimized version of **read.table**.

```{r}
# ShortStack mapping results for each sample
WT <- fread("WT_sRNA_shortstack/Results.txt", header = T)
nrpd1 <- fread("nrpd1_sRNA_shortstack/Results.txt", header = T)
nrpe1 <- fread("nrpe1_sRNA_shortstack/Results.txt", header = T)

# We have to add a column to differentiate the files before combining them
WT[, Sample := "WT"]
nrpd1[, Sample := "nrpd1"]
nrpe1[, Sample := "nrpe1"]

# If the tables have the same number of columns, we can concat them by row with the rbindlist function
# Let's combine the tables together
?rbindlist
shortstack.data <- rbindlist(list(WT, nrpd1, nrpe1))

#View(shortstack.data)
```

## Data tidying and processing

The first important step in every R analysis is to prepare, clean and format correctly the input data.
It is especially necessary when you collect data from different people, formats, online sources, etc...

In our case, the files are already in the same format (the ShortStack output format) but we still need to arrange some columns.

Here are some very useful functions:
```{r}
# Split a column to multiple columns 
shortstack.data[, c("Type", "ID", "Name") := tstrsplit(Name, ";")] # it splits Name by ";" and create 3 new distinct columns: Type, ID, Name

# Remove unwanted character
shortstack.data[, Name := gsub("Name=", "", Name)] 
shortstack.data[, ID := gsub("ID=", "", ID)] 

# Rename a column: e.g "#Locus" column to "Locus""
setnames(shortstack.data, "#Locus", "Locus")
```

Let's see the clean data.table

```{r, eval=FALSE}
shortstack.data
```

```{r, eval=TRUE, echo=FALSE }
library(DT)
datatable(head(shortstack.data, 50), extensions = 'FixedColumns', options = list(
  searching = TRUE,
  pageLength = 5,
  lengthMenu = c(5, 10, 15, 20),
  dom = 't',
  scrollX = TRUE,
  scrollCollapse = TRUE
))
```

#### Select specific column in our data 

To reduce the data size and improve calculation time, we are going to select the columns we are interested in and create a new subset.

```{r, warning=FALSE, message=FALSE}

# It is really easy with the select function
?select
shortstack.subset <- dplyr::select(shortstack.data, Name, ID, Type, Sample, "20", "21", "22", "23", "24")
```

#### Compute Read per Million Mapped (RPM)

By default the RPM computed by ShortStack takes in account all reads in a region and normalized it by the whole library size (not only the mapped).
We will recompute here the RPM for each sRNA categories (from 20->24) using the total mapped reads.

It would be easier (and faster) for us to have only one column to apply the RPM. 
The idea is to have all the sRNAs sizes (21 to 24) grouped in one column named "Size"
and their counts in another column "Count"

We are going to tidy up the "shortstack.subset" with the melt function.
http://garrettgman.github.io/tidying/

**melt** takes wide-format data and melts it into long-format data
(if you want more explanation about http://seananderson.ca/2013/10/19/reshape.html)

Note: The long format is also the format required by ggplot2

```{r}
?melt
# transform wide data to long format 
shortstack.subset <- melt(shortstack.subset, 
                          id.vars = c("Name", "ID", "Type", "Sample"), 
                          variable.name = "Size", 
                          value.name = "Count")
```

Here is the vector of total reads mapped in the libraries

```{r}
lib.size <- c(WT = 5725918, nrpd1 = 4995744, nrpe1 = 3361162)
lib.size
```

Now that the counts are in the same column in **shortstack.subset**, we can apply the RPM normalization.
We will add a column containing the RPM rounded to three digits.

```{r}
# Add a new RPM colum to the shortstack.subset datatable
shortstack.subset[, RPM := round(Count * 1000000 / lib.size[Sample], 3)]
```

- Which loci has the highest RPM?
- Which TE has the highest RPM?

#### Construction of a ggplot:  minimum required arguments

```r
ggplot(data, aes(x, y)) + geom_type() 
```

**data** is a data.table or data.frame.  
**x** and **y** are precise columns in your table, which will represent the x-axis and y-axis.  
**geom_type** is the type of graphics you want (e.g geom_boxplot, geom_point, geom_bar, etc...)  

See here for the different type of geom plot: https://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf

#### Example of Boxplot creation

We want a boxplot showing the RPM of miRNA and TE from our samples.
The x-axis will be the sizes (21, 22, etc..) and the y-axis the freshly new RPM calculated!

```{r }
ggplot(shortstack.subset, aes(x = Size, y = RPM)) + geom_boxplot()
```

The representation is not so great, we should use a log scale instead.
You can directly add a log scale to the RPM variable in a ggplot command.

```{r }
?log
ggplot(shortstack.subset, aes(x = Size, y = log2(RPM + 1))) + geom_boxplot()

# Reorder by specific order in a plot
shortstack.subset$Sample <- factor(shortstack.subset$Sample, levels=c("WT", "nrpd1", "nrpe1"))
shortstack.subset$Type <- factor(shortstack.subset$Type, levels=c("miRNA", "transposable_element"))

# Difference between miRNA and TE
ggplot(shortstack.subset, aes(x = Size, y = log2(RPM + 1), fill = Type)) + geom_boxplot()

# And difference between sample
ggplot(shortstack.subset, aes(x = Size, y = log2(RPM + 1), fill = Type)) + geom_boxplot() + 
  facet_wrap(~ Sample, ncol = 1)
```

That's better now !

#### Plot sRNA distribution by size

We want to see the count of sRNAs by sample, by size and by type (miRNA or TE)
We need the sum of the counts and the sum of RPM by sample, size, and type

We can do that in one line and generate a new datatable

```{r, message=FALSE, warning=FALSE}
dt.count <- shortstack.subset[, list(Count = sum(Count), RPM = sum(RPM)), by = c("Sample", "Size", "Type")]
```

#### Barplot of sRNA size

Let's see the differences in term of sRNA size between our samples

```{r, warning=FALSE}
# with RPM
ggplot(dt.count, aes(x = Size, y = RPM, fill = Sample)) + 
  geom_bar(stat = "identity", position = "dodge")
```

Once you have your data in the correct format, with very little changes you can adapt the plot to what you want
Here are 3 differents ways of representing our data

```{r, warning=FALSE}
ggplot(dt.count, aes(x = Size, y = RPM, fill = Type)) + geom_bar(stat = "identity", position = "dodge") + 
  facet_wrap(~ Sample, ncol = 1)

ggplot(dt.count, aes(x = Size, y = RPM, fill = Sample)) + geom_bar(stat = "identity", position = "dodge") + 
  facet_wrap(~ Type, ncol = 1)

ggplot(dt.count, aes(x = Size, y = RPM, fill = Sample)) + geom_bar(stat = "identity", position = "dodge") + 
  facet_grid(Sample ~ Type)
```

#### Example of sophisticated plot 

```{r, warning=FALSE }
# usefull package
library(scales) # package for dealing with axis format
library(RColorBrewer) # package with pre-defined color palette

ggplot(dt.count, aes(x = Size, y = RPM, fill = Sample)) + geom_bar(stat = "identity", position = "dodge") + 
  scale_y_continuous(breaks=pretty_breaks(n=8), labels = comma) + # improved scale for the y-axis
  facet_grid(Sample ~ Type) + # Results by sample and by type 
  theme_bw() + # black and white theme (it removes the grey background that annoys a lot of people)
  scale_fill_brewer(palette="Dark2") + # apply the nice Dark2 theme from the Rcolorbrewer package
  ggtitle("sRNA size distribution") + # add a title
  xlab("sRNA by size") + # change the x-axis name
  ylab("Reads per millions mapped") # change the y-axis name

# Display the different color palette for 3 variables
# display.brewer.all(3, colorblindFriendly = T)
```

## Analysis of ShortStack's results with miRNA de novo detection  

```{r}
# ShortStack mapping with De novo detection of miRNA
WT.denovo <- fread("~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_analysis/ShortStack/Denovo_miRNA_identification/WT_sRNA_miRNAsearch/Results.txt", header = T)
nrpd1.denovo <- fread("~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_analysis/ShortStack/Denovo_miRNA_identification/nrpd1_sRNA_miRNAsearch/Results.txt", header = T)
nrpe1.denovo <- fread("~/Desktop/IBMP_Bioinformatics_Workshop/DAY2/sRNAseq_analysis/ShortStack/Denovo_miRNA_identification/nrpe1_sRNA_miRNAsearch/Results.txt", header = T)

WT.denovo[, Sample := "WT"]
nrpd1.denovo[, Sample := "nrpd1"]
nrpe1.denovo[, Sample := "nrpe1"]

# If the tables have the same number of columns, we can concat them by row with the rbindlist function
?rbindlist
shortstack.denovo.data <- rbindlist(list(WT.denovo, nrpd1.denovo, nrpe1.denovo))

# We can filter now the MIRNA detected (if they passed all ShortStack's validation steps)
miRNA.detected <- shortstack.denovo.data[MIRNA == "Y"]
View(miRNA.detected)

miRNA.detected <- miRNA.detected[miRNA.detected[, .I[which.max(`21`)], by=c("MajorRNA", "Sample")]$V1]

table(miRNA.detected$DicerCall) 
table(miRNA.detected$Sample)

# Change the order of the samples, we want WT first, then nrpd1 and nrpe1
miRNA.detected[, Sample := factor(Sample, levels=c("WT", "nrpd1", "nrpe1"))]

ggplot(miRNA.detected, aes(x = MajorRNA, y = log2(RPM + 1), fill = Sample)) + geom_bar(stat = "identity") + 
  coord_flip() + xlab("miRNA detected by ShortStack") + facet_wrap(~ Sample) 

# Example of code on how to add a weblink to a table
# Here we link the sequence of the MajorRNA to the blast application on mirbase21.
library(DT)
miRNA.detected[, mirbase_blast := paste0('<a href="http://www.mirbase.org/cgi-bin/blast.pl?sequence=', MajorRNA, '" target="_blank">blast_mirbase21</a>')]
datatable(dplyr::select(miRNA.detected, Sample, RPM, `21`, MajorRNA, mirbase_blast), escape = FALSE)
```


