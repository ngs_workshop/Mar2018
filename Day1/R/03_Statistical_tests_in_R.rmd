---
title: 'Statistical Tests'
author: "Timothée Vincent & David Pflieger"
output:
  pdf_document: default
  html_document: default
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = '~/Desktop/IBMP_Bioinformatics_Workshop/DAY1/R/data')
```

```{r}
library(data.table)
setwd("~/Desktop/IBMP_Bioinformatics_Workshop_2017/DAY1/R/data")
iris <- fread("iris.csv")
```

### How to test normality of data

##### 1. Shapiro-Wilk & qqplot 

The **Shapiro-Wilk** test is probably the most widely used test to check normality. In R the function to perform this test is **shapiro.test()**.  
It takes only one argument as input: a numeric vector. 

A **qqplot** is a visual representation of the scatterplot of the two sets of quantiles against one another. 
If both sets of quantiles came from the same distribution, we should see the points forming a line that’s roughly straight.
The R function are **qqnorm()** and **qqline()**, it takes the same input as the Shapiro-wilk.

Example:

```{r}
?shapiro.test

set.seed(123)
normal.distrib <- rnorm(100, mean = 5, sd = 3) # create a vector with values normally distributed
plot(density(normal.distrib)) # check visually 

set.seed(123)
non.normal.distrib <- runif(100, min = 2, max = 5) # create a vector of random values, ot should not be normally distributed
plot(density(non.normal.distrib))

# Testing for normality
shapiro.test(normal.distrib) # Accept H0: data follow a normal distribution
shapiro.test(non.normal.distrib) # Not enough evidence to support H0
# The p-value tells us the chances that the sample comes from a normal distribution. 
# The lower the pvalue is, the smaller the chance

# Check visually
qqnorm(normal.distrib)
qqline(normal.distrib, col = "red")

qqnorm(non.normal.distrib)
qqline(non.normal.distrib, col = "red")
```

#### Exercise: check if the iris variable **Sepal.Width** follow a normal distribution in setosa and virginica

### 2. How to test the equality of variances?

Some tests like the classic **Student’s t-test** (comparing two independent samples) and the **ANOVA** test (comparing multiple samples) assume that the samples to be compared follow a normal and have equal variances.

To test the variances: 

F-test: Compare the variances of two samples
Bartlett’s test: Compare the variances of n samples, where n can be more than two samples
Levene’s test: same as Bartlett's test but less sensitive to data that deviate from a normal distribution

Example:
```{r}
# F-test with the var.test() function
?var.test
var.test(iris[Species == "setosa"]$Sepal.Width, iris[Species == "virginica"]$Sepal.Width)

# barlett's test
?bartlett.test
bartlett.test(Sepal.Width ~ Species, data = iris)

# install.packages("car")
library(car) # The Levene's test is not implemented in base R, we have to load a package
leveneTest(Sepal.Width ~ Species, data = iris)
```

### 3. Compare one sample to a fixed average

**T-test** (for parametric data) / **Wilcoxon signed rank** (non-parametric data)

Also one of the most common tests in statistics. 
It is used to determine whether the means of two groups are equal to each other. 
The assumption for the test is that both groups are sampled from normal distributions with equal variances. 
The null hypothesis is that the two means are equal, and the alternative is that they are not. 

t.test(x, mu = 0) where x is the vector and mu the mean specified by your null hypothesis.

Let's take the measures of ten plants (in cm), we want to see if they have a mean equal to 50 cm. 
So our null hypothesis H0: mean = 50. And the alternative hypothesis (H1) is : mean no equal to 50.

```{r}
size <- c(47, 51, 48, 49, 48, 52, 47, 49, 46, 47, 45)

# Does this sample follow a normal law? 
shapiro.test(size)
qqnorm(size)
qqline(size)

# H0: mu is equal to 50
t.test(size, mu = 50)

# H1; mu is greater/lower than 50
t.test(size, mu = 50, alternative = c("less"))
t.test(size, mu = 50, alternative = c("greater"))

# Wilcoxcon 
wilcox.test(size, mu = 50)
```

### 4. One-way ANOVA

ANOVA stands for ANalysis Of VAriance. It is a test to determine if there is a significant difference/variance among group means. 
The null hypothesis states that all population means are equal while the alternative hypothesis states that at least one is different. 
The One-way ANOVA is used to test groups with only one response variable.

```{r}
library(ggplot2)
# Check visually Sepal.Width distribution 
ggplot(iris, aes(x = Sepal.Width, color = Species, fill = Species)) + geom_density(alpha = 0.3)
ggplot(iris, aes(Species, Sepal.Width, fill = Species)) + geom_boxplot()

# We tested the variance and normality before

# Perform an ANOVA test with the aov() function
?aov

ANOVA <- aov(Sepal.Width ~ Species, data=iris)
summary(ANOVA) # View results of the ANOVA test
# P-value is less than our alpha 0.05 significance level. 
# We reject H0, and accept the alternative hypothesis that the differences between at least one of the means is statistically significant

# Next, we can run post-hoc tests, such as Tukey's Honest Significant Difference test, to determine which species have significantly different means.
?TukeyHSD
TukeyHSD(ANOVA) 
# The padj-value is less than our alpha in all three pairwise comparisons, meaning there is a significant difference between all three species' means. 


# If you prefer t.tests, you can also perform multiple pairwise t-test and choose a p-value adjustments method
pairwise.t.test(iris$Sepal.Width, iris$Species, 
                p.adj="bonferroni", paired=FALSE)
# Same results as Tukey, the p-values adjusted are less than 0.05
```

### 5. Correlation test

Pearson correlation (r), which measures a linear dependence between two variables (x and y). 
Also known as a parametric correlation test, it depends to the distribution of the data. 

Kendall tau and Spearman rho, which are rank-based correlation coefficients (non-parametric)

The R functions:

cor(x, y, method = c("pearson", "kendall", "spearman"))
cor.test(x, y, method=c("pearson", "kendall", "spearman")) # returns the pvalue too

```{r}
# Check the correlation between our iris variables
cor.test(iris$Sepal.Length, iris$Sepal.Width, method = "pearson") # We accept H0, there is no correlation (correlation is not significatively different of 0)
ggplot(iris, aes(Sepal.Length, Sepal.Width)) + geom_point() + geom_smooth(method = "lm")

cor.test(iris$Sepal.Length, iris$Petal.Length, method = "pearson") # We cannot accept H0, there is a correlation (correlation is significatively different of 0)
ggplot(iris, aes(Sepal.Length, Petal.Length)) + geom_point() + geom_smooth(method = "lm")

cor.test(iris$Petal.Length, iris$Petal.Width, method = "pearson") 
ggplot(iris, aes(Petal.Length, Petal.Width)) + geom_point() + geom_smooth(method = "lm")
```




